color desert
syntax on
" Formatting
set tabstop=4       " Number of displayed spaces per TAB
set softtabstop=4   " Number of spaces in edit mode per TAB
set expandtab       " Turn tabs into spaces
